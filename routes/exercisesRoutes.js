const mongoose = require("mongoose");
const requireLogin = require("../middlewares/requireLogin");
const requireAdmin = require("../middlewares/requiredAdmin");
const Exercise = mongoose.model("exercises");

module.exports = app => {

  //returns exercises list
  app.get('/api/exercises', requireLogin, requireAdmin, async (req, res) => {
    const exercises = await Exercise.find();
    res.send(exercises);
  })

  //creates a exercise
  app.post("/api/exercises", requireLogin, requireAdmin, async (req, res) => {
    const { exerciseName, videoDemo } = req.body;
    const exercise = await new Exercise({
      exerciseName,
      videoDemo
    }).save((error, exercise) => {
      if (error) {
        res.status(404).send({ error: "That exercise already exists" })
      } else {
        console.log(exercise);
        res.status(201).send({ success: "Exercise Created", exercise });
      }
    });
  });

  app.delete("/api/exercise/:id", async (req, res) => {
    const { id } = req.params;
    const exercise = await Exercise.deleteOne({ _id: id }, (error) => {
      if (error) {
        res.status(404).send({ error: "Exercise not deleted" });
      } else {
        res.status(201).send({ success: "Exercise deleted" })
      }
    })

  })
};