const validate = ({ rutineTitle, rutineUser, rutineWeek }) => {
  // create the error object
  const errors = {}
  // title and user validation
  if (!rutineTitle || rutineTitle === undefined) {
    errors.rutineTitle = 'Required'
  }
  if (!rutineUser || rutineUser === undefined) {
    errors.rutineUser = 'Select an user from the list'
  }
  // rutine week validation 
  if (!rutineWeek || rutineWeek === undefined) {
    errors.rutineWeek = { _error: "Rutine Days must have a section or be rest day" }
  } else {
    //  days array errors created
    const daysArrayErrors = []
    // looping thrw the week to validate the data in the days, sections and excersices
    rutineWeek.forEach((day, dayIndex) => {
      // daysErrors object and sectionError array
      const dayErrors = {}
      const sectionArrayError = []
      // day sections validation
      if (day.sections === undefined || !day.sections || !day.sections.length) {
        dayErrors.sections = { _error: 'Day needs at least one section' }
      } else {
        // validating if is rest day or not
        if (!day.isRestDay) {
          // looping thrw day sections 
          day.sections.forEach((section, sectionIndex) => {
            const sectionErrors = {}
            const exercisesArrayError = []
            // validating the section exist
            if (section !== undefined || !section || !section.length) {
              // type and email validation
              if (section.type === undefined || !section.type) {
                sectionErrors.type = 'Required'
                sectionArrayError[sectionIndex] = sectionErrors
              }
              if (section.time === undefined || !section.time) {
                sectionErrors.time = 'Required'
                sectionArrayError[sectionIndex] = sectionErrors
              }
              // validating if has excercises row with data
              if (section.exercises === undefined || !section.exercises || !section.exercises.length) {
                sectionErrors.exercises = { _error: 'Section needs at least one excersice' }
              } else {
                //  looping thrw exercises
                // eslint-disable-next-line array-callback-return
                section.exercises.map((exercise, exerciseIndex) => {
                  // exercise error object
                  const exerciseErrors = {}
                  // validating if index exist
                  if (exerciseIndex !== undefined) {
                    // validating if exercise row exist
                    if (exercise === undefined) {
                      exerciseErrors.name = 'Required'
                      exerciseErrors.reps = 'Required'
                      exerciseErrors.tool = 'Required'
                      exerciseErrors.weight = 'Required'
                      exercisesArrayError[exerciseIndex] = exerciseErrors
                    } else {
                      // name, reps, tool and weight inputs validation
                      if (exercise.name === undefined) {
                        exerciseErrors.name = 'Required'
                        exercisesArrayError[exerciseIndex] = exerciseErrors
                      }
                      if (exercise.reps === undefined || !exercise.reps) {
                        exerciseErrors.reps = 'Required'
                        exercisesArrayError[exerciseIndex] = exerciseErrors
                      }
                      if (exercise.tool === undefined || !exercise.tool) {
                        exerciseErrors.tool = 'Required'
                        exercisesArrayError[exerciseIndex] = exerciseErrors
                      }
                      if (exercise.weight === undefined || !exercise.weight) {
                        exerciseErrors.weight = 'Required'
                        exercisesArrayError[exerciseIndex] = exerciseErrors
                      }
                    }
                  }
                })
                //  adding the exercise error array with the errors to the section object
                sectionErrors.exercises = exercisesArrayError
              }
            }
            // adding the section object to the section array with errors
            sectionArrayError[sectionIndex] = sectionErrors
          })
        }
        //  adding section array to day errors object
        dayErrors.sections = sectionArrayError
      }
      //  adding day object to day array errors
      daysArrayErrors[dayIndex] = dayErrors
    })
    // adding the day array errors to the object of errors at rutineWeek 
    errors.rutineWeek = daysArrayErrors
  }
  // returning all the errors stack
  return errors
}

export default validate