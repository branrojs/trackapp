import { useState, useEffect } from "react";
import { submitExercise, fetchExercises, deleteExercise } from "../actions";
import { useDispatch, useSelector } from "react-redux";
import { useAlert } from "react-alert";

const useExerciseController = () => {
  //variables block
  const [exerciseName, setExerciseName] = useState("");
  const [videoDemo, setvideoDemo] = useState("");
  const [filterName, setFilterName] = useState("");
  const { exercises, loading } = useSelector((state) => state.exercise); // create the reducer and bring the action back
  const dispatch = useDispatch();
  const alerts = useAlert();

  //use effect to dispatch fetchExercises
  useEffect(() => {
    dispatch(fetchExercises());
  }, [dispatch]);

  // submitHandler
  const submitHandler = (e, exerciseName, videoDemo) => {
    e.preventDefault();
    if (
      exerciseName === undefined ||
      exerciseName === "" ||
      videoDemo === undefined ||
      videoDemo === ""
    ) {
      alerts.error("Exercise Name and video Demo can not be empty");
    } else {
      if (
        exercises.some((exercise) => {
          return exercise.exerciseName === exerciseName;
        })
      ) {
        alerts.error("That exercise already exists");
      } else {
        dispatch(submitExercise({ exerciseName, videoDemo }));
        alerts.success("Succesfully added");
        setExerciseName("");
        setvideoDemo("");
      }
    }
  };

  const exerciseDelete = (e, id) => {
    e.preventDefault();
    dispatch(deleteExercise(id));
    alerts.success("Successfully Deleted");
  };

  // return everything
  return {
    exerciseName,
    setExerciseName,
    videoDemo,
    setvideoDemo,
    filterName,
    setFilterName,
    submitHandler,
    exerciseDelete,
    exercises,
    loading,
  };
};

export default useExerciseController;
