import { FETCH_RUTINES_SUCCESS, FETCH_RUTINES_REQUEST, FETCH_RUTINES_FAIL } from '../actions/types';

export default function (state = { rutines: [] }, action) {
  switch (action.type) {
    case FETCH_RUTINES_REQUEST:
      return { loading: true };
    case FETCH_RUTINES_SUCCESS:
      return { loading: false, rutines: action.payload };
    case FETCH_RUTINES_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}