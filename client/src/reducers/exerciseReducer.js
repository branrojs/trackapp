import {
  FETCH_EXERCISES_SUCCESS,
  FETCH_EXERCISES_REQUEST,
  FETCH_EXERCISES_FAIL,
  POST_EXERCISES_FAIL,
  POST_EXERCISES_SUCCESS,
  POST_EXERCISES_REQUEST,
  DELETE_EXERCISES_FAIL,
  DELETE_EXERCISES_SUCCESS,
  DELETE_EXERCISES_REQUEST,
} from "../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (
  state = { loading: true, exercises: [], success: {} },
  action
) {
  switch (action.type) {
    case FETCH_EXERCISES_REQUEST:
      return { loading: true };
    case FETCH_EXERCISES_SUCCESS:
      return { loading: false, exercises: action.payload };
    case FETCH_EXERCISES_FAIL:
      return { loading: false, error: action.payload };
    case POST_EXERCISES_REQUEST:
      return { loading: false, exercises: [...state.exercises] };
    case POST_EXERCISES_SUCCESS:
      return {
        loading: false,
        exercises: [...state.exercises, action.payload.exercise],
        success: action.payload.success,
      };
    case POST_EXERCISES_FAIL:
      return {
        loading: false,
        exercises: [...state.exercises],
        error: action.payload,
      };
    case DELETE_EXERCISES_REQUEST:
      return { loading: false, exercises: state.exercises };
    case DELETE_EXERCISES_SUCCESS:
      return {
        loading: false,
        exercises: [
          ...state.exercises.filter(
            (exercise) => exercise._id !== action.payload.exerciseId
          ),
        ],
        success: action.payload.success,
      };
    case DELETE_EXERCISES_FAIL:
      return {
        loading: false,
        exercises: [...state.exercises],
        error: action.payload,
      };
    default:
      return state;
  }
}
