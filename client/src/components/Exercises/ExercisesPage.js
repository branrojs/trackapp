import React from "react";
import useExerciseController from "../../Custom Hooks/useExerciseController";

import AddExercises from "./AddExercises";
import ExercisesList from "./ExercisesList";

const ExercisesPage = () => {
  const {
    exercises,
    loading,
    filterName,
    setFilterName,
    exerciseDelete,
    ...exerciseController
  } = useExerciseController();

  return (
    <div className="row">
      <div className="col s12 m12 l12">
        <h2 className="center">Exercises list</h2>
        <AddExercises {...exerciseController} />
        <ExercisesList
          exercises={exercises}
          loading={loading}
          filterName={filterName}
          setFilterName={setFilterName}
          exerciseDelete={exerciseDelete}
        />
      </div>
    </div>
  );
};

export default ExercisesPage;
