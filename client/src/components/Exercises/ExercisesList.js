import React from "react";
import Message from "../Message";

import Skeleton from "@material-ui/lab/Skeleton";

const ExercisesList = ({
  exercises,
  loading,
  filterName,
  setFilterName,
  exerciseDelete,
}) => {
  return (
    <div className="row ">
      <div className="col s12 m12 l12">
        <div className="input-field col s4 m4 l4">
          <i className="material-icons prefix">filter_list</i>
          <input
            id="name_filter"
            type="text"
            className="validate"
            value={filterName}
            onChange={(e) => setFilterName(e.currentTarget.value)}
          />
          <label htmlFor="name_filter">Search by name</label>
        </div>
        {loading && loading ? (
          <Skeleton />
        ) : exercises && exercises.length !== 0 ? (
          <table className="striped">
            <thead>
              <tr>
                <th>Exercise Name</th>
                <th>Video Help</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {exercises
                .filter((exercise) => {
                  return exercise.exerciseName.includes(filterName);
                })
                .map(({ _id, exerciseName, videoDemo }) => {
                  return (
                    <tr key={_id}>
                      <td>{exerciseName}</td>
                      <td>{videoDemo}</td>
                      <th>
                        <button
                          className="btn waves-effect waves-light red"
                          onClick={(e) => exerciseDelete(e, _id)}
                        >
                          <i className="material-icons prefix">
                            delete_forever
                          </i>
                        </button>
                      </th>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        ) : (
          <Message error="No existe ningun ejercicio ingresado en esta lista, agregue uno porfavor" />
        )}
      </div>
    </div>
  );
};

export default ExercisesList;
