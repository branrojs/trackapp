import React from "react";

const Message = ({ error }) => {
  return (
    <div className="col s12 m12 l12 center">
      <h3>{error}</h3>
    </div>
  );
};
export default Message;
