import _ from "lodash";
import React from "react";
import { SocialIcon } from "react-social-icons";

const socialMedias = [
  "http://instagram.com/",
  "http://twitter.com/",
  "http://facebook.com/",
];

const Footer = () => {
  const socialLinks = _.map(socialMedias, (value) => {
    return (
      <SocialIcon
        url={value}
        key={value}
        target="_blank"
        className="white-text"
      />
    );
  });

  return (
    <footer className="page-footer grey darken-3 mt-mid2">
      <div className="container">
        <div className="row">
          <div className="col l5 s12">
            <h5 className="white-text center-align">Big Logo</h5>
          </div>
          <div className="col l5 offset-l2 s12">
            <h5 className="white-text">Check out our social media</h5>
            {socialLinks}
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
            {/*eslint-disable-next-line*/}© 2020 Copyright Text{" "}
            <a href="" className="grey-text text-lighten-4 right">
              Sorrow the prog!
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
