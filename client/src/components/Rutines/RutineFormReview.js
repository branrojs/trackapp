import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions/index';
import Days from './Rutine/Days';

const RutineFormReview = ({ onCancel, rutineData, submitRutine, history }) => {
  const { _id, rutineTitle, rutineUser, rutineWeek } = rutineData;
  return (
    <div className="row">
      <div className="col s12">
        <h5>Please confirm your entries</h5>
        <div>
          <div className="col s12" key={_id}>
            <div className=" grey lighten-4 hoverable" >
              <div className="card-content">
                <h4 className="card-title center-align">
                  {rutineTitle  /*change*/}
                </h4>
                <span className="left-align">User: {rutineUser}</span>
              </div>
              <div className="card-action">
                <div className="card-content">
                  <Days rutineDays={rutineWeek} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bottons">
        <button
          className="yellow darken-3 white-text btn-flat"
          onClick={onCancel} >
          Back
      </button>
        <button
          className="green white-text btn-flat right"
          onClick={() => submitRutine(rutineData, history)}>
          Create rutine
        <i className="material-icons right">assignment_turned_in</i>
        </button>
      </div>

    </div>
  )
}

function mapStateToProps(state) {
  return { rutineData: state.form.rutineForm.values };
}


export default connect(
  mapStateToProps, actions)(withRouter(RutineFormReview));
