import React, { useEffect } from 'react'
import { connect, useSelector, useDispatch } from 'react-redux';
import { fetchRutine } from '../../../actions'; //change 
import Days from './Days';
import { Icon, Button } from 'react-materialize';

const Rutine = () => {

  const rutineData = useSelector(state => state.rutine);
  const { rutine, loading } = rutineData;
  const dispatch = useDispatch();

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    if (params.has('id')) {
      dispatch(fetchRutine(params.get('id')));
    }
  }, [dispatch]);

  return (
    <div className="mt-mid mb-mid">
      <div className="row">
        <Button node="button" style={{ marginRight: '5px' }} waves="light" onClick={() => window.history.go(-1)}>Go back<Icon left>arrow_back</Icon>
        </Button>
      </div>
      <div className="row">
        {loading ?
          <div className="mt-big mb-big">
            <div className="progress">
              <div className="indeterminate"></div>
            </div>
          </div>
          :
          rutine.map(({ _id, rutineTitle, rutineWeek }) => {
            return (
              <div className="col s12" key={_id}>
                <div className=" grey lighten-4 hoverable" >
                  <div className="card-content">
                    <h4 className="card-title center-align">
                      {rutineTitle  /*change*/}
                    </h4>

                  </div>
                  <div className="card-action">
                    <div className="card-content">
                      <Days rutineDays={rutineWeek} />
                    </div>
                  </div>
                </div>
              </div>

            )
          })
        }
      </div>
    </div>
  )
}

function mapStateToProps({ rutines }) {
  return { rutines }; //change
}

export default connect(mapStateToProps, { fetchRutine })(Rutine);