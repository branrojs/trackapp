import React from "react";
import { Link } from "react-router-dom";

const Clients = ({ client }) => {
  return (
    <div className="col s12 m6 l6 xl4">
      <div className="card grey lighten-4 hoverable">
        <div className="card-content">
          <span className="card-title">{client.userName /*change*/}</span>
          <p className="right">Srw atlethe</p>
        </div>
        <div className="card-action">
          <Link
            to={"/rutines?user=" + client.userName.replaceAll(" ", "_")}
            className="waves-effect"
          >
            View rutines
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Clients;
