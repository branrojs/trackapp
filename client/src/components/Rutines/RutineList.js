import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { fetchRutines } from "../../actions"; //change
import { Icon, Button } from "react-materialize";
import Message from "../Message";

const RutineList = (props) => {
  const rutinesData = useSelector((state) => state.rutines);
  const { rutines, loading } = rutinesData;
  const dispatch = useDispatch();

  useEffect(() => {
    //obtains the params from the url and validate them
    const params = new URLSearchParams(window.location.search);

    if (params.has("user")) {
      dispatch(fetchRutines(params.get("user").replaceAll("_", " ")));
    }
  }, [dispatch]);
  return (
    <div className="mt-mid">
      <div className="row">
        <Button
          node="button"
          style={{ marginRight: "5px" }}
          waves="light"
          onClick={() => window.history.go(-1)}
        >
          Go back<Icon left>arrow_back</Icon>
        </Button>
      </div>
      <div className="row">
        {loading && loading ? (
          <div className="mt-big">
            <div className="progress">
              <div className="indeterminate"></div>
            </div>
          </div>
        ) : rutines && rutines.length !== 0 ? (
          rutines.map((rutine) => {
            return (
              <div className="col col s12 m6 l6 xl4" key={rutine._id}>
                <div className="card grey lighten-4 hoverable">
                  <div className="card-content">
                    <span className="card-title">
                      {rutine.rutineTitle /*change*/}
                    </span>
                    <p className="right">{rutine.userRutine}</p>
                  </div>
                  <div className="card-action">
                    <Link
                      to={"/rutine?id=" + rutine._id}
                      className="waves-effect"
                    >
                      View rutine
                    </Link>
                  </div>
                </div>
              </div>
            );
          })
        ) : (
          <Message error="Este usuario no tiene ninguna rutina ingresada" />
        )}
      </div>
      <div className="filler-content-2"></div>
    </div>
  );
};

export default RutineList;
