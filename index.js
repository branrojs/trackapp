const express = require("express");
const mongoose = require("mongoose");
const cookieSession = require("cookie-session");
const passport = require("passport");
const bodyParser = require("body-parser");
const keys = require("./config/keys");
const morgan = require("morgan");
const helmet = require("helmet");
require("./models/User");
require("./models/Survey");
require("./services/passport");
require("./models/Rutine");
require("./models/Exercises");

mongoose.connect(keys.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, autoIndex: true });

mongoose.connection.on('error', (err) => {
  console.log('db.error' + err);
});

const app = express();

app.use(bodyParser.json());
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
app.use(morgan('tiny'));
//app.use(helmet());
app.use(passport.initialize());
app.use(passport.session());

require("./routes/authRoutes")(app);
require("./routes/billingRoutes")(app);
require("./routes/surveyRoutes")(app);
require("./routes/rutineRoutes")(app);
require("./routes/clientRoutes")(app);
require("./routes/exercisesRoutes")(app);

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'PRODUCTION') {
  app.use(express.static('client/build'));
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

app.listen(process.env.PORT || 5000);
