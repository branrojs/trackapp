const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema({
  googleId: String,
  userName: String,
  userEmail: String,
  userPicture: String,
  credits: {type: Number, default: 0},
  admin: {type: String, default: "false"}
});

mongoose.model("users", userSchema);
